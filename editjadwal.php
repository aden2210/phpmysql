<?php
    include 'connect.php';

    $data = mysqli_query($conn, "SELECT * FROM tb_jadwal WHERE id_jadwal = '".$_GET['id']."'");
    $r = mysqli_fetch_array($data);

    $id_dosen = $r['id_dosen'];
    $id_kelas = $r['id_kelas'];
    $jadwal = $r['jadwal'];
    $matakuliah = $r['matakuliah'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>

<div class="container-fluid px-1 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <h3>Input Kelas</h3>
            <div class="card">
                <form class="form-card" action="" method="POST" enctype="multipart/form-data">
                <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Id Dosen<span class="text-danger"> *</span></label> <input type="text"  name="id_dosen" value="<?php echo $id_dosen ?>" placeholder="Enter your id dosen" onblur="validate(1)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Id Kelas<span class="text-danger"> *</span></label> <input type="text" name="id_kelas" value="<?php echo $id_kelas ?>" placeholder="Enter your id kelas" onblur="validate(2)"> </div>
                    
                    </div>
                    <div class="row justify-content-between text-left">
                    <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Jadwal<span class="text-danger"> *</span></label> <input type="date"  name="jadwal" value="<?php echo $jadwal ?>" placeholder="" onblur="validate(4)"> </div>
                        
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Mata Kuliah<span class="text-danger"> *</span></label> <input type="text"  name="matakuliah" value="<?php echo $matakuliah ?>" placeholder="" onblur="validate(4)"> </div>
                    </div>
                </form>
            </div>
            <div class="row justify-content-around">
        <div class="col-4">
        <a class="btn btn-outline-primary " href="datajadwal.php" role="button">Data</a>
        </div>
        <div class="col-4">
        <a class="btn btn-outline-dark " href="index.php" role="button">Home</a>
        </div>
    </div>
        </div>
    </div>
    
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <?php
    
    if(isset($_POST['kirim'])){
        $id_dosen = $_POST['id_dosen'];
        $id_kelas = $_POST['id_kelas'];
        $jadwal = $_POST['jadwal'];
        $matakuliah = $_POST['matakuliah'];
    
       
       if($nama_kelas != ''){
           $update = mysqli_query($conn, "UPDATE tb_jadwal SET 
           id_dosen = '". $id_dosen."',
          id_kelas = '". $id_kelas."',
          jadwal= '". $jadwal."',
          matakuliah = '". $matakuliah."',
           
           WHERE id_jadwal = '".$_GET['id']."'
           ");
        if($update){
            echo 'Berhasil Upadate';
        }else{
            echo 'Gagal Upadate';
        }
       }
    }

    ?>

  </body>
</html>