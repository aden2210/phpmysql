<?php
    include 'connect.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>
  <div class="container border rounded border-info border-3 shadow-sm mt-5 mb-5 p-5">
        <h1 class="text-center">Input Kelas</h1>
        <div class="row justify-content-center ">
            <div class="col-8 p-5">
            <form action="" method="POST">
                    <div class="mb-3">
                    <label for="nama" class="form-label">Nama Kelas</label>
                    <input type="text"  name="nama_kelas" placeholder="" onblur="validate(1)">
                    </div>
                    <div class="mb-3">
                    <label for="mapel" class="form-label">Prodi</label>
                    <input type="text"  name="prodi_kelas" placeholder="" onblur="validate(3)">
                    </div>
                    <div class="mb-3">
                    <label for="uts" class="form-label">Fakultas</label>
                    <input type="text"  name="fakultas_kelas" placeholder="" onblur="validate(4)">
                    </div>
                    <button type="submit" class="btn-block btn-primary" name="kirim">Save</button>
                </form>
            </div>
        </div>
    </div>
    <div class="row justify-content-around">
    <div class="col-4">
    <a class="btn btn-outline-primary " href="datakelas.php" role="button">Data</a>
    </div>
    <div class="col-4">
    <a class="btn btn-outline-dark " href="index.php" role="button">Home</a>
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <?php
    
    if(isset($_POST['kirim'])){
       $nama_kelas = $_POST['nama_kelas'];
       $prodi_kelas = $_POST['prodi_kelas'];
       $fakultas_kelas = $_POST['fakultas_kelas'];
       
       
       $insert = mysqli_query($conn, "INSERT INTO tb_kelas VALUES (
           NULL,
           '$nama_kelas',
           '$prodi_kelas',
           '$fakultas_kelas'
           
       )");

       if($insert){
           echo 'Upload Berhasil';
       }else{
           echo 'Upload Gagal';
       }

    }

    ?>

  </body>
</html>