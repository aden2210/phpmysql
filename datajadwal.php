<?php
    include 'connect.php';
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>

    
    <div class="container-fluid px-1 py-5 mx-auto">
    <h1 class="text-center">Data Jadwal</h1>
    <div class="container ">
    <table class="table table-dark table-striped " >
        <tr>
            <td class="col-md-2">Id_Dosen</td>
            <td class="col-md-2">Id_Kelas</td>
            <td class="col-md-2">Jadwal</td>
            <td class="col-md-2">MataKuliah</td>
            <td class="col-md-2">Aksi</td>
        </tr>
            <?php
                $query = mysqli_query($conn, "SELECT * FROM tb_jadwal");
                while($row = mysqli_fetch_array($query)){
            ?>
        <tr>
            <td class="col-md-2"><?php echo $row['id_jadwal'] ?></td>
            <td class="col-md-2"><?php echo $row['id_kelas'] ?></td>
            <td class="col-md-2"><?php echo $row['jadwal'] ?></td>
            <td class="col-md-2"><?php echo $row['matakuliah'] ?></td>
            <td class="col-md-2">
                <a class="btn btn-outline-primary" href="editjadwal.php?id=<?php echo $row['id_jadwal'] ?>" role="button">Edit</a> 
                <a class="btn btn-outline-danger" href="hapusjadwal.php?id=<?php echo $row['id_jadwal'] ?>" role="button">Delete</a>
            </td>
        </tr>
        <?php } ?>
    </table>
    <div class="row justify-content-around">
        <div class="col-4">
        <a class="btn btn-outline-primary " href="inputjadwal.php" role="button">Input</a>
        </div>
        <div class="col-4">
        <a class="btn btn-outline-dark " href="index.php" role="button">Home</a>
        </div>
    </div>
    </div>
    </div>

    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    
  </body>
</html>