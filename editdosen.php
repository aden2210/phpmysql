<?php
    include 'connect.php';

    $data = mysqli_query($conn, "SELECT * FROM tb_dosen WHERE id_dosen = '".$_GET['id']."'");
    $r = mysqli_fetch_array($data);

    $nama = $r['nama'];
    $nip = $r['nip'];
    $prodi = $r['prodi'];
    $fakultas = $r['fakultas'];
    $file = $r['file'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>

<div class="container-fluid px-1 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <h3>Input Dosen</h3>
            <div class="card">
                <form class="form-card" action="" method="POST" enctype="multipart/form-data">
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Nama<span class="text-danger"> *</span></label> <input type="text"  name="nama" value="<?php echo $nama ?>" placeholder="Enter your first name" onblur="validate(1)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">NIP<span class="text-danger"> *</span></label> <input type="text" name="nip" value="<?php echo $nip ?>" placeholder="Enter your nip" onblur="validate(2)"> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Prodi<span class="text-danger"> *</span></label> <input type="text"  name="prodi" value="<?php echo $prodi ?>" placeholder="" onblur="validate(3)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Fakultas<span class="text-danger"> *</span></label> <input type="text"  name="fakultas" value="<?php echo $fakultas ?>" placeholder="" onblur="validate(4)"> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-12 flex-column d-flex"> <label class="form-control-label px-3">Upload Profile Picture<span class="text-danger"> *</span></label> <input type="hidden" name="img" value="<?php echo $file ?>"> <input type="file"  name="gambar" placeholder="" onblur="validate(6)"> </div>
                    </div>

                    <div class="row justify-content-between text-left">
                        <div class="form-group col-12 flex-column d-flex"> <label class="form-control-label px-3"> <img src="upload/<?php echo $file ?>"style="width: 120px;"/></div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="form-group col-sm-6"> <button type="submit" class="btn-block btn-primary" name="kirim">Update</button> </div>
                    </div>
                </form>
            </div>
            <div class="row justify-content-around">
        <div class="col-4">
        <a class="btn btn-outline-primary " href="datadosen.php" role="button">Data</a>
        </div>
        <div class="col-4">
        <a class="btn btn-outline-dark " href="index.php" role="button">Home</a>
        </div>
    </div>
        </div>
    </div>
    
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <?php
    
    if(isset($_POST['kirim'])){
       $nama = $_POST['nama'];
       $nama_file = $_FILES['gambar']['name'];
       $source = $_FILES['gambar']['tmp_name'];
       $folder = './upload/';
       $nip = $_POST['nip'];
       $prodi = $_POST['prodi'];
       $fakultas = $_POST['fakultas'];
    
       
       if($nama_file != ''){
           move_uploaded_file($source,$folder.$nama_file);
           $update = mysqli_query($conn, "UPDATE tb_dosen SET 
           nama = '".$nama."',
           file = '".$nama_file."', 
           nip = '".$nip."',
           prodi = '".$prodi."',
           fakultas = '".$fakultas."'
           
           WHERE id_dosen = '".$_GET['id']."'
           ");
        if($update){
            echo 'Berhasil Upadate';
        }else{
            echo 'Gagal Upadate';
        }
       }else{
        $update = mysqli_query($conn, "UPDATE tb_dosen SET 
        nama = '".$nama."'
        nip = '".$nip."',
        prodi = '".$prodi."',
        fakultas = '".$fakultas."',
        WHERE id_dosen = '".$_GET['id']."'
        ");
        if($update){
            echo 'Berhasil Upadate';
        }else{
            echo 'Gagal Upadate';
        }
       }
       
    }

    ?>

  </body>
</html>