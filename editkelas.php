<?php
    include 'connect.php';

    $data = mysqli_query($conn, "SELECT * FROM tb_kelas WHERE id_kelas = '".$_GET['id']."'");
    $r = mysqli_fetch_array($data);

    $nama_kelas = $r['nama_kelas'];
    $prodi_kelas = $r['prodi_kelas'];
    $fakultas_kelas = $r['fakultas_kelas'];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link rel="stylesheet" href="style.css">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>

<div class="container-fluid px-1 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <h3>Input Kelas</h3>
            <div class="card">
                <form class="form-card" action="" method="POST" enctype="multipart/form-data">
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Nama<span class="text-danger"> *</span></label> <input type="text"  name="nama_kelas" value="<?php echo $nama_kelas ?>" placeholder="Enter your first name" onblur="validate(1)"> </div>
                    </div>
                    <div class="row justify-content-between text-left">
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Prodi<span class="text-danger"> *</span></label> <input type="text"  name="prodi_kelas" value="<?php echo $prodi_kelas ?>" placeholder="" onblur="validate(3)"> </div>
                        <div class="form-group col-sm-6 flex-column d-flex"> <label class="form-control-label px-3">Fakultas<span class="text-danger"> *</span></label> <input type="text"  name="fakultas_kelas" value="<?php echo $fakultas_kelas ?>" placeholder="" onblur="validate(4)"> </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="form-group col-sm-6"> <button type="submit" class="btn-block btn-primary" name="kirim">Update</button> </div>
                    </div>
                </form>
            </div>
            <div class="row justify-content-around">
        <div class="col-4">
        <a class="btn btn-outline-primary " href="datakelas.php" role="button">Data</a>
        </div>
        <div class="col-4">
        <a class="btn btn-outline-dark " href="index.php" role="button">Home</a>
        </div>
    </div>
        </div>
    </div>
    
</div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <?php
    
    if(isset($_POST['kirim'])){
       $nama_kelas = $_POST['nama_kelas'];
       $prodi_kelas = $_POST['prodi_kelas'];
       $fakultas_kelas = $_POST['fakultas_kelas'];
    
       
       if($nama_kelas != ''){
           $update = mysqli_query($conn, "UPDATE tb_kelas SET 
           nama_kelas = '".$nama_kelas."',
           prodi_kelas = '".$prodi_kelas."',
           fakultas_kelas = '".$fakultas_kelas."'
           
           WHERE id_kelas = '".$_GET['id']."'
           ");
        if($update){
            echo 'Berhasil Upadate';
        }else{
            echo 'Gagal Upadate';
        }
       }
    }

    ?>

  </body>
</html>