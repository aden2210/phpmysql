<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Penjadwalan Dosen</title>
  </head>
  <body>

<div class="container border rounded border-info border-3 shadow-sm mt-5 mb-5 p-5">
        <h1 class="text-center">PENJADWALAN</h1>
        <div class="row justify-content-center ">
            <div class="col-8 p-5">
                <form action="" method="POST">
                <div class="row justify-content-around">
                <div class="col-4">
                <a class="btn btn-outline-primary " href="inputdosen.php" role="button">Dosen</a>
                </div>
                <div class="col-4">
                <a class="btn btn-outline-primary " href="inputjadwal.php" role="button">Jadwal Kelas</a>
                </div>
                <div class="col-4">
                <a class="btn btn-outline-primary " href="inputkelas.php" role="button">Kelas</a>
                </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>
